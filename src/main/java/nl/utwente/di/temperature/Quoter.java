package nl.utwente.di.temperature;

public class Quoter {
    double getTemperature(String temperature){
        double temp = Double.parseDouble(temperature)*1.8 + 32;
        return temp;
    }
}
